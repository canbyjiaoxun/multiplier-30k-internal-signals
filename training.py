from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model, neighbors, datasets
import pandas as pd
import numpy as np
from sklearn import cross_validation
from sklearn import svm
from sklearn.metrics import accuracy_score


DATA = pd.read_csv('./stat_train/stat_ST0.FPMultiplier_8_23_8_23_8_23_uid2_Wrapper.0.85.50.0.80.txt',header=None)
X_train  = np.array(DATA)     #read traing data
train_target  = np.array(pd.read_csv('./stat_train/stat_ST1.FPMultiplier_8_23_8_23_8_23_uid2_Wrapper.0.85.50.0.80.txt',header=None)) #read training target

DATA2 = pd.read_csv('./stat_test/stat_ST0.FPMultiplier_8_23_8_23_8_23_uid2_Wrapper.0.85.50.0.80.txt',header=None)
X_test  = np.array(DATA2)     #read test data
test_target  = np.array(pd.read_csv('./stat_test/stat_ST1.FPMultiplier_8_23_8_23_8_23_uid2_Wrapper.0.85.50.0.80.txt',header=None))   #read test target
f = open('./predictor.txt', 'w')   #output prediction result file

len(test_target[0]) #number of columns



err_rate = float(0)
evaluation = 0.0 
f.write("bit\t");
f.write("prediction accuracy\t");
f.write("evaluation score\t");
f.write("error count\n");
for j in range(0, len(test_target[0])):   #for each column
#for j in range(0, 10):   #for each column
    error = 0
    y_train=np.zeros(len(train_target))   
    y_test=np.zeros(len(test_target))
    for i in range(0,len(train_target)):  #for each row 
        y_train[i]=train_target[i,][j]
        y_test[i]=test_target[i,][j]
        if y_test[i] == 1:                #count number of error in the column 
            error = error + 1
        
    if error > 0:                         #only do learning when there are errors 
       print 'error count', error, 
       err_rate = error/len(train_target)    #error rate in the column
       print 'error rate:', err_rate,
       print 'length of rows:', len(train_target),
       model = neighbors.KNeighborsClassifier(n_neighbors=1, leaf_size=68)   
      # model = svm.SVC()
      # model = linear_model.LogisticRegression()
       model.fit(X_train, y_train)
       X = model.predict(X_test)
       result = (np.sum(y_test==X)+0.000000000000001)/len(X)     #prediction accuracy
       evaluation = (err_rate-(1-result))/err_rate               #evaluation factor: [p-(1-accuracy)]/p, where p is error rate at some bit position
       print 'score', evaluation
       f.write(repr(j))
       f.write("\t")
       f.write(repr(result)) 
       f.write("\t")
       f.write(repr(evaluation))
       f.write("\t")
       f.write(repr(error))
       f.write("\n")

f.close()